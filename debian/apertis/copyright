Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: aclocal.m4
 config.rpath
Copyright: 1996-2014, Free Software Foundation, Inc.
License: FSFULLR

Files: common/clone_ptr-t.hpp
 common/clone_ptr.hpp
 common/copy_ptr.hpp
 common/enumeration.hpp
 common/generic_copy_ptr-t.hpp
 common/generic_copy_ptr.hpp
 common/hash-t.hpp
 common/hash.hpp
 common/hash_fun.hpp
 common/lock.hpp
 common/lsort.hpp
Copyright: 2000-2004, 2011, Kevin Atkinson
License: NTP

Files: common/gettext.h
Copyright: 1995-1998, 2000-2002, Free Software Foundation, Inc.
License: LGPL-2+

Files: compile
 depcomp
 missing
Copyright: 1995-2013, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL

Files: debian/*
Copyright: 1999-2001 Sudhakar Chandrasekharan <thaths@netscape.com>
            2001-2002 Domenico Andreoli <cavok@debian.org>
            2002-2018 Brian Nelson <pyro@debian.org>
            2014-2018 Agustín Martín Domingo <agmartin@debian.org>
License: LGPL-2.1+

Files: debian/patches/01_debctrl_filter.diff
Copyright: 2005, Brian Nelson, based on the email filter
 2001, Kevin Atkinson under the GNU LGPL license
License: LGPL-2.1+

Files: debian/patches/03_ddtp_filter.diff
Copyright: 2007, Christer Andersson <klamm@comhem.se>
License: LGPL-2+

Files: debian/prezip.1
Copyright: (co 2002, Kevin Atkinson.
License: LGPL-2.1+

Files: install-sh
Copyright: 1994, X Consortium
License: X11

Files: ltmain.sh
Copyright: 1996-2001, 2003-2011, Free Software Foundation, Inc.
License: GPL-2+ with Libtool exception

Files: m4/*
Copyright: 1996-2014, Free Software Foundation, Inc.
License: FSFULLR

Files: m4/gettext.m4
 m4/intl.m4
 m4/intldir.m4
 m4/intlmacosx.m4
 m4/po.m4
 m4/progtest.m4
Copyright: 1995-2014, Free Software Foundation, Inc.
License: FSFULLR and/or GPL and/or LGPL

Files: m4/glibc2.m4
 m4/glibc21.m4
 m4/ltoptions.m4
Copyright: 2000-2002, 2004, 2005, 2007-2014, Free Software Foundation
License: FSFULLR

Files: m4/lcmessage.m4
 m4/nls.m4
Copyright: 1995-2006, 2008-2014, Free Software Foundation
License: FSFULLR and/or GPL and/or LGPL

Files: m4/libtool.m4
Copyright: 1996-2001, 2003-2011, Free Software
License: (FSFULLR and/or GPL-2+) with Libtool exception

Files: manual/*
Copyright: 2000-2019, Kevin Atkinson.
License: GFDL-1.1+

Files: manual/aspell-dev.texi
Copyright: @copyright{} 2002, 2003, 2004, 2006 Kevin Atkinson.
License: GFDL-1.1+

Files: manual/aspell.html/Copying.html
Copyright: 2000-2019, Kevin Atkinson.
License: GFDL-1.1+ and/or LGPL

Files: manual/aspell.html/GNU-Lesser-General-Public-License.html
Copyright: 2000-2019, Kevin Atkinson.
License: GFDL-1.1+ and/or LGPL-2.1

Files: manual/aspell.info
Copyright: 2000-2019, Kevin Atkinson.
License: GFDL-1.1+ and/or LGPL

Files: manual/aspell.texi
Copyright: @copyright{} 2000-2019, Kevin Atkinson.
License: GFDL-1.1+ and/or LGPL

Files: manual/lgpl.texi
Copyright: @copyright{} 1991, 1999 Free Software Foundation, Inc.
License: LGPL-2.1

Files: manual/mdate-sh
Copyright: 1995-2013, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: manual/texinfo.tex
Copyright: 1985, 1986, 1988, 1990-2013, Free Software Foundation, Inc.
License: GPL-3+

Files: misc/*
Copyright: 2000, 2001, Sergey Poznyakoff
License: GPL-2+

Files: modules/filter/ddtp.cpp
Copyright: 2007, Christer Andersson <klamm@comhem.se>
License: LGPL-2+

Files: modules/filter/nroff.cpp
Copyright: 2004, Sergey Poznyakoff
License: LGPL-2.1+

Files: modules/speller/*
Copyright: 2000-2005, 2011, Kevin Atkinson under the terms of the LGPL
License: LGPL

Files: modules/speller/default/block_vector.hpp
 modules/speller/default/primes.cpp
 modules/speller/default/primes.hpp
 modules/speller/default/vector_hash-t.hpp
 modules/speller/default/vector_hash.hpp
Copyright: 2000-2004, 2011, Kevin Atkinson
License: NTP

Files: modules/speller/default/phonet.cpp
 modules/speller/default/phonet.hpp
Copyright: 2000, Björn Jacke
License: LGPL-2.1

Files: modules/speller/default/speller_impl.hpp
Copyright: 1998-2000, Kevin Atkinson under the terms of the LGPL.
License: LGPL

Files: prog/compress.c
 prog/prezip.c
Copyright: 2000-2004, 2011, Kevin Atkinson
License: NTP

Files: scripts/*
Copyright: 2000-2004, 2011, Kevin Atkinson
License: NTP

Files: test/*
Copyright: 2000, BjÃ¶rn Jacke
License: LGPL-2.1

Files: * Makefile.in common/* common/can_have_error.hpp common/document_checker.cpp common/document_checker.hpp common/error.hpp common/errors.cpp common/errors.hpp common/filter.cpp common/filter.hpp common/filter_debug.hpp common/key_info.hpp common/mutable_container.hpp common/speller.cpp common/string_map.hpp common/string_pair.hpp common/string_pair_enumeration.hpp common/strtonum.cpp common/strtonum.hpp common/suggestions.hpp common/type_id.hpp common/word_list.hpp examples/Makefile.in lib/new_filter.cpp lib/new_fmode.cpp lib5/* modules/* modules/filter/context.cpp modules/filter/debctrl.cpp modules/filter/sgml.cpp modules/speller/default/affix.cpp modules/speller/default/affix.hpp modules/speller/default/check_list.hpp modules/speller/default/data.cpp modules/speller/default/readonly_ws.cpp modules/speller/default/speller_impl.cpp modules/speller/default/writable.cpp myspell/* myspell/munch.c prog/* scripts/aspell-import
Copyright: 2002-2018 Kevin Atkinson <kevina@gnu.org>
License: LGPL-2.1+

Files: manual/Makefile.in manual/aspell.1 manual/fdl.texi
Copyright: 2002-2018 Kevin Atkinson <kevina@gnu.org>
License: GFDL-1.2+
